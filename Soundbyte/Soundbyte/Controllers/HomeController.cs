﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Soundbyte.Models;

namespace Soundbyte.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        public ActionResult Index(string q)
        {
            if (!string.IsNullOrWhiteSpace(q))
            {
                var imageResult = ImageResult(q);
                var topResult = TopResult(q);

                return View(new ResultModel
                {
                    Image = imageResult,
                    SearchTerm = q,
                    AudioSrc = $"{topResult}?client_id={ConfigurationManager.AppSettings["soundcloud:ClientId"]}"
                });
            }

            return View("Search");
        }

        private GiphyEmbedImage ImageResult(string q)
        {
            return new GiphyEmbedImage(ApiResult("http://api.giphy.com/",
                $"v1/gifs/search?q={q}&api_key={ConfigurationManager.AppSettings["giphy:key"]}&limit=1"));
        }

        private string TopResult(string q)
        {
            const string durationClause = "&duration[from]=500&duration[to]=15100";
            const string genresClause = "&genres=sound effects,sound effect,soundeffects,soundeffect";

            return
                Search(q, durationClause + genresClause) ??
                Search(q, durationClause) ??
                Search(q, genresClause)??
                Search(q);
        }

        private string Search(string q, string urlParams = "")
        {
            var searchResults = ApiResult<IList<Track>>("http://api.soundcloud.com/",
                $"tracks.json?client_id={ConfigurationManager.AppSettings["soundcloud:ClientId"]}&q={q}&filters=streamable{urlParams}");
            return !searchResults.Any() ? null : searchResults.First().stream_url;
        }

        private dynamic ApiResult(string urlHost, string url)
        {
            return JObject.Parse(ApiResult<string>(urlHost, url));
        }

        private T ApiResult<T>(string urlHost, string url) where T : class
        {
            var searchResponse = new RestClient(urlHost).Get(new RestRequest(url));

            if (typeof(T) == typeof(string))
            {
                return searchResponse.Content as T;
            }

            var searchResults = JsonConvert.DeserializeObject<T>(searchResponse.Content);
            return searchResults;
        }
    }
}