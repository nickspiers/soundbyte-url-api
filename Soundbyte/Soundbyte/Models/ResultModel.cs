namespace Soundbyte.Models
{
    public class ResultModel
    {
        public string SearchTerm { get; set; }
        public string AudioSrc { get; set; }
        public GiphyEmbedImage Image { get; set; }
    }

    public class GiphyEmbedImage
    {
        private readonly string _string;

        public GiphyEmbedImage(dynamic response)
        {
            var dataArray = response?.data;

            if ((dataArray?.Count ?? 0) > 0)
            {
                var data = dataArray[0];

                if (data != null)
                {
                    var url = data.embed_url;
                    var original = data.images.original;
                    var width = original.width;
                    var height = original.height;
                    _string =
                        $"<iframe src='{url}' width='{width}' height='{height}' frameBorder='0' allowFullScreen></iframe>";
                }
            }
        }

        public override string ToString()
        {
            return _string;
        }
    }
}